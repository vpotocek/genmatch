#include "common.h"

#define M 1000
#define G 1000

/* compute all fitnesses, save them for later use, compute and output average,
 * mean, minimum. Return value = position of the minimum. */
int computeFitnesses(point ptsFrom[], point ptsTo[], candidate c[]) {
  double fit, sf = 0, sf2 = 0, min = 1E10;
  int i, pmin = 0;
  for(i = 0; i < M; i++) {
    fit = fitness(ptsFrom, ptsTo, &c[i]);
    c[i].fitness = fit;
    if(fit < min) {
      min = fit;
      pmin = i;
    }
    sf += fit;
    sf2 += fit*fit;
  }
  printf("fitness %lf ± %lf, min %lf\n",
      sf/M, sqrt(sf2/M-sf/M*sf/M), min);
  return pmin;
}

/* http://rosettacode.org/wiki/Sorting_algorithms/Quicksort#C */
void qsort_pop(candidate **arr, int n) {
  double p;
  int i, j;
  candidate *t;
  if(n < 2) return;
  p = arr[n/2]->fitness;
  for(i = 0, j = n - 1;; i++, j--) {
    while(arr[i]->fitness < p) i++;
    while(p < arr[j]->fitness) j--;
    if(i >= j) break;
    t = arr[i];
    arr[i] = arr[j];
    arr[j] = t;
  }
  qsort_pop(arr, i);
  qsort_pop(arr + i, n - i);
}

void computeRanks(candidate c[]) {
  candidate* order[M];
  int i, j, t;
  for(i = 0; i < M; i++)
    order[i] = &c[i];
  qsort_pop(order, M);
  for(i = 0; i < M; i++)
    order[i]->rank = i+1;
}

/* roulette wheel selection / reproduction */
void selection(candidate src[], candidate dest[]) {
  int i;
  double bounds[M+1], max;
  bounds[0] = 0;
  for(i = 0; i < M; i++)
    bounds[i+1] = bounds[i] + 1/src[i].rank; // lower rank preferred
  max = bounds[M];

  /* find the slice corresponding to a number picked uniformly from [0,max)
   * using binary search */
  for(i = 0; i < M; i++) {
    double pick = drand48()*max;
    int mn = 0, mx = M, tg;
    while(mx - mn > 1) {
      tg = (mn+mx)/2; /* guaranteed to be different from either */
      if(bounds[tg] > pick)
        mx = tg;
      else
        mn = tg;
    }
    /* bounds[mn] <= pick && bounds[mn+1] >= pick */
    dest[i] = src[mn];
  }
}

void crossover(candidate c[], double pC) {
  /* random pairing replaced by random selection earlier */
  int i, tmp[N];
  for(i = 0; i < M; i += 2)
    if(drand48() < pC) {
      int x = (int)(drand48()*N);
      memcpy(tmp, c[i].map, sizeof(c[i].map));
      memcpy(c[i].map + x, c[i+1].map + x, N-x);
      memcpy(c[i+1].map + x, tmp + x, N-x);
    }
}

void mutation(candidate c[], double pM) {
  int i;
  for(i = 0; i < M; i++)
    if(drand48() < pM) {
      int x = (int)(drand48()*N);
      /* can result in no change - that's OK as it's a probabilistic operation anyway */
      c[i].map[x] = x + (int)(drand48()*(N-x));
    }
}

int main() {
  srand48(time(NULL));
  point pts[2][N];
  candidate pop[M], pop2[M];
  int i, j, g, pmin;

  FILE *f = fopen("pts.data", "rb");
  fread(pts, sizeof(point), 2*N, f);
  fclose(f);
  
  for(i = 0; i < M; i++)
    for(j = 0; j < N; j++)
      pop[i].map[j] = j + (int)(drand48()*(N-j));

  for(g = 0; g < G; g++) {
    printf("%3i: ", g);
    computeFitnesses(pts[0], pts[1], pop); /* finishes the output line */
    computeRanks(pop);
    selection(pop, pop2);
    mutation(pop2, 0.2);
    crossover(pop2, 0.6);
    memcpy(pop, pop2, sizeof(pop));
    fflush(stdout);
  }

  printf("%3i: ", g);
  pmin = computeFitnesses(pts[0], pts[1], pop); /* pmin = best candidate */
  f = fopen("conn.data", "wb");
  decodeperm(pop[pmin].map);
  fwrite(pop[pmin].map, sizeof(int), N, f);
  fclose(f);

  return 0;
}
