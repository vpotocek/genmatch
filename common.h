#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#define N 100

typedef struct {
  double x;
  double y;
} point;

typedef struct {
  int map[N];
  double fitness;
  int rank;
} candidate;

double fitness(point ptsFrom[], point ptsTo[], candidate *c);

/* Create a permutation using the Fisher-Yates shuffling algorithm. */
void decodeperm(int map[]);

/* Encode a permutation into a series of successive shuffles. */
void encodeperm(int map[]);
