# Introduction

This is a simple demo of a genetic algorithm and simulated annealing, both 
presented the same problem of finding an optimal perfect matching in 
a complete bipartite graph with weighted edges. More specifically, two sets of 
100 points each are scattered randomly in a 1×1 square and the task is to find 
a mapping where the distances from each preimage to its image are as small as 
possible. Both the algorithms do so by starting with a random permutation, or 
a population made thereof, and optimizing stochastically.


## Details

The permutations are encoded using the [Fisher–Yates 
scheme](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle), in which 
each permutation is reached by transposing element 1 with eny element, 
followed by transposing element 2 with any except 1, etc. This way each 
permutation of N elements is uniquely described by a string of integers where 
the j-th number lies between j and N, and conversely, any such string is 
a legitimate description of a permutation.

The genetic implementation starts with M = 1000 random permutations, which are 
subject to a generational algorithm using rank-based selection, one-point 
crossover, and 1-gene random-allele mutation. The probabilities at the time of 
writing of this file are 0.6 for crossover and 0.2 for mutation. One thousand 
generations are taken although the algorithm usually reaches a local optimum 
within 200 generations.

The simulated annealing is run for a total of 5×10⁵ steps, using fitness (see 
below) as energy and the number of steps, downscaled by 4 orders of magnitude,
as the Boltzmann factor (corresponding to a temperature decreasing 
harmonically to from infinity to 1/50 in units corresponding to the fitness 
function). A mutation of the same kind as above is offered at each step. It is 
accepted automatically if it decreases fitness but subject to a probabilistic 
acceptance if it does not. In its initial phases the algorithm is likely to 
accept any mutations but the criterion becomes progressively more strict. 
However, because of a comparison with a randomly generated bound, a big 
upwards jump can in principle happen at any point.

The fitness function, common to both parts, is the total distance of all the 
connections between the two sets of points. A simple nonlinear transform 
(square root) is applied on the result to favour better solutions more 
significantly even in comparison with good-enough ones.

The random real numbers are directly generated using the 48-bit `POSIX` 
library in `stdlib.h`, see [`srand48(3)`][rand48] and [`drand48(3)`][rand48].


## Dependencies and invocation

The programs can be run using a single `make` command, which generates three 
executables, `generate`, `gen-alg` and `annealing`, pictures `gen-alg.png` and 
`annealing.png` and text files `gen-alg.out` and `annealing.out`. A plot 
comparing the fitness evolution is presented on screen.

This requires the following programs to be installed and present in `$PATH`:

* `gcc`

* `WolframScript` (part of Wolfram Mathematica 10+)

* `gnuplot`


## Results

The following images showcase the results for one run on a randomly generated 
set of points, as of 22/6/2016.

The result of the genetic algorithm after 1000 generations:

![GA result](https://bytebucket.org/vpotocek/genmatch/raw/master/sample-output/gen-alg.png)

The result of simulated annealing after 5×10⁵ steps:

![SA result](https://bytebucket.org/vpotocek/genmatch/raw/master/sample-output/annealing.png)

The comparison of the fitness in the two methods, rescaled in x to the same 
extent:

![comparison](https://bytebucket.org/vpotocek/genmatch/raw/master/sample-output/comparison.png)

[rand48]: http://linux.die.net/man/3/drand48
