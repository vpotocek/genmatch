pts = ArrayReshape[#, {2, Length[#]/4, 2}] & @ Import["pts.data", "Real64"];
conn = Import["conn.data", "Integer32"];
Export["plot.png", Graphics[{PointSize[Medium],
  {Blue, Point[pts[[1]]]},
  {Red, Point[pts[[2]]]},
  {Gray, Arrowheads[Small], Arrow[Thread[{pts[[1]], pts[[2, conn + 1]]}]]}
}]]
(*vim:syn=mathematica*)
