#include "common.h"

double dist(point *a, point *b) {
  return hypot(b->x-a->x, b->y-a->y);
}

/* nonlinear transform to emphasize differences between near-optimal solutions */
double fitnesstrans(double x) {
  return sqrt(x);
}

/* total distance a point j in arg1 to a point at c->map[j] in arg2 */
double fitness(point ptsFrom[], point ptsTo[], candidate *c) {
  int i;
  float totaldist = 0;
  decodeperm(c->map);
  for(i = 0; i < N; i++)
    totaldist += dist(&ptsFrom[i], &ptsTo[c->map[i]]);
  encodeperm(c->map);
  return fitnesstrans(totaldist);
}

/* Create a permutation using the Fisher-Yates shuffling algorithm. */
void decodeperm(int map[]) {
  int i, j, tmp;
  int dec[N];
  for(i = 0; i < N; i++)
    dec[i] = i;
  for(i = 0; i < N; i++) {
    j = map[i];
    tmp = dec[i];
    dec[i] = dec[j];
    dec[j] = tmp;
  }
  memcpy(map, dec, sizeof(dec));
}

/* Encode a permutation into a series of successive shuffles. */
void encodeperm(int map[]) {
  int i, j;
  int enc[N];
  for(i = 0; i < N; i++)
    enc[i] = i;
  for(i = 0; i < N; i++) {
    j = map[i];
    while(enc[j] != j)
      j = enc[j];
    enc[i] = j;
  }
  memcpy(map, enc, sizeof(enc));
}
