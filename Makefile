GCC = gcc
MATH = WolframScript

PROGRAMS = gen-alg annealing
SOURCES = $(PROGRAMS) generate
RUN = $(addprefix run-, $(PROGRAMS))

all:	gen run

.PHONY:	gen run

$(SOURCES): %: %.c
	$(GCC) -lm -o $@ $@.c common.c

gen:	generate
	./generate

run: $(RUN)
	gnuplot -p plot.gp

$(RUN): run-%: %
	./$< | tee $<.out
	$(MATH) -script analyze.m
	mv plot.png $<.png
