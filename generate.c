#include "common.h"

int main() {
  srand48(time(NULL));
  point pts[2][N];
  int i, j;
  for(j = 0; j < 2; j++)
    for(i = 0; i < N; i++) {
      pts[j][i].x = drand48();
      pts[j][i].y = drand48();
    }

  FILE *f = fopen("pts.data", "wb");
  fwrite(pts, sizeof(point), 2*N, f);
  fclose(f);

  return 0;
}
