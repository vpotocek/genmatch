#include "common.h"

#define G 500000

void mutation(candidate *cIn, candidate *cOut) {
  memcpy(cOut, cIn, sizeof(candidate));
  int x = (int)(drand48()*(N-1));
  /* guarantee a mutation: disallow the previous value */
  int prev = cOut->map[x];
  cOut->map[x] = x + (int)(drand48()*(N-1-x));
  if(cOut->map[x] >= prev) cOut->map[x]++;
}

int main() {
  srand48(time(NULL));
  point pts[2][N];
  candidate c, cTemp;
  int j;

  FILE *f = fopen("pts.data", "rb");
  fread(pts, sizeof(point), 2*N, f);
  fclose(f);

  for(j = 0; j < N; j++)
    c.map[j] = j + (int)(drand48()*(N-j));
  c.fitness = fitness(pts[0], pts[1], &c);

  for(j = 0; j < G; j++) {
    mutation(&c, &cTemp);
    cTemp.fitness = fitness(pts[0], pts[1], &cTemp);
    double beta = j/10000;
    /* decrease always succeeds, increase exponentially less likely with j */
    if(exp(beta*(c.fitness - cTemp.fitness)) > drand48()) {
      printf("%3i: %lf → %lf\n", j, c.fitness, cTemp.fitness);
      memcpy(&c, &cTemp, sizeof(c));
      fflush(stdout);
    }
  }

  f = fopen("conn.data", "wb");
  decodeperm(c.map);
  fwrite(c.map, sizeof(int), N, f);
  fclose(f);

  return 0;
}
